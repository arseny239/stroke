В данном проекте необходимо предсказать инсульт у пациента по клиническим признакам из датасета Kaggle

Используется датасет с 11 клиническими признаками.
Необходимо по 10 признакам спрогнозировать, существует ли опасность возникновения инсульта.

Ссылка на датасет Kaggle:
https://www.kaggle.com/datasets/fedesoriano/stroke-prediction-dataset

Ссылка на Google Colab:
https://colab.research.google.com/drive/1b9HM0FvU7LKdybtL9VO1uB3ZaIJnpaym?usp=sharing